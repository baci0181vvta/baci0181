package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.Assert.*;



class EmployeeMockTest {



    @Test
    void modifyEmployeeFunctionTC1() throws EmployeeException {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee Ionel = employeeMock.getEmployeeList().get(0);
        System.out.println(employeeList.get(0));
        employeeMock.modifyEmployeeFunction(Ionel, DidacticFunction.LECTURER);

        Ionel = employeeMock.getEmployeeList().get(0);
        assertNotEquals(DidacticFunction.ASISTENT, Ionel.getFunction());
        assertEquals(DidacticFunction.LECTURER, Ionel.getFunction());
        System.out.println(employeeList.get(0));

    }

    @Test
    void modifyEmployeeFunction_TC2() throws EmployeeException {
        EmployeeMock employeeMock = new EmployeeMock(0);
        List<Employee> employeeList = employeeMock.getEmployeeList();

        assertEquals(0, employeeList.size());
        System.out.println("Angajati in lista: " + employeeList.size());
    }


    @Test
    void modifyEmployeeFunctionTC3() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee Ionel = null;
        List<Employee> employeeMockList = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(Ionel, DidacticFunction.TEACHER);
        assertTrue(employeeMock.getEmployeeList().equals(employeeMockList));
        System.out.println(employeeMockList.get(0));
    }
}