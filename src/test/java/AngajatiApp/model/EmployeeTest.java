package AngajatiApp.model;

import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    private String firstName;
    private Employee e1, e2, e3;

    @BeforeEach
    void setUp() {
        e1 = new Employee();
        e2 = null;
        e3 = new Employee();
        e3.setFirstName("Ana");
        System.out.println("Before test");
    }

    @AfterEach
    void tearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("After test");
    }
    @Order (1)
    @Test
    public void TestEmployee() {
        assertAll(
                () ->assertEquals("", e1.getFirstName()),
                () ->assertEquals(null, e2),
                () -> assertEquals("Ana", e3.getFirstName())

        );
    }
    @Order (2)
    @Test
    void getFirstName_simple1() {
        assertEquals("Ana", e3.getFirstName());
        System.out.println("E3 has first name Ana");
    }

    @Test
    void getFirstName_simple2(){
        try{
            e2.getFirstName();
            assert(false);}
        catch (Exception e){
            System.out.println(e.toString());
            assert(true);
        }
        System.out.println("e2 doesn't have First Name");
    }
    @Order (3)
    @Test
    void getFirstName_compus(){
        assertEquals("Ana", e3.getFirstName());
        assertEquals("", e1.getFirstName());
        try {
            assertEquals("", e2.getFirstName());
        } catch (Exception e) {
            assert(true);
        }
        System.out.println("3 get First Name Tests");
    }
    @Test
    void getFgetFirstName_simple4(){
        assertThrows(NullPointerException.class,()->e2.getFirstName());
        System.out.println("e2 does not have First Name");

    }

    @Test
    void getFirstName_testAll(){
        assertAll("Employee",
                ()-> assertEquals("Ana", e3.getFirstName()),
                ()-> assertEquals("", e1.getFirstName()),
                ()-> assertNull(e2));
        System.out.println("Asert all 3 tests ok");
    }
    @Test
    void setFirstName() {
        assertEquals("", e1.getFirstName());
        e1.setFirstName("Luca");
        assertNotEquals("", e1.getFirstName());
        assertEquals("Luca", e1.getFirstName());
        System.out.println("e1 First Name is Luca");
    }
    @Order (4)
    @Test
    void constrEmployee(){
        Employee aux=new Employee();
        assertEquals(e1.getFirstName(), aux.getFirstName());
        assertNotEquals(null, aux);
        System.out.println("Constructor test works");
    }

    @Disabled
    @Test
    void getFirstName_Disabled() {
        assertEquals(" ", e1.getFirstName());
        System.out.println("e1 doesn't have first name");
    }

    @Test
//@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
//@Timeout(5)
    public void searchEmployeeFromString()  {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(e1.equals("Ana"), false);
        System.out.println("Search employee from string with timeout ok");
    }
    @Test
    public void searchEmployeeFromString_cu_assertTimeout() {
        Assertions.assertTimeout(Duration.ofSeconds(10), () -> {
            delay_and_call();
        }, String.valueOf(false));
        System.out.println(" assertTimeout ok");
    }

    private void delay_and_call() throws EmployeeException {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ;
        e1.equals("Ana");
        System.out.println("Test cauta Ana Ok");
    }
    @ParameterizedTest
    @ValueSource(strings = {"Mihai", "Ana", "Radu"})
    void testParametrizatSetandGetFirstName(String firstName) {
        e1.setFirstName(firstName);
        assertEquals(firstName, e1.getFirstName());
        System.out.println("Parametrized Set&Get with "+firstName+" ok");
    }


}