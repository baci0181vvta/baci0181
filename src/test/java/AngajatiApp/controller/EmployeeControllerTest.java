package AngajatiApp.controller;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {
    EmployeeImpl ec;

    @BeforeEach
    void setUp() {

        ec = new EmployeeImpl ();

    }
    @Test
    void addEmployeeTC1_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("Ionel");
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(6000d);
        try { ec.addEmployee(e);
            assertEquals(employeeNo + 1, ec.getEmployeeList().size());
        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC2_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("Ionel");
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(999d);
      try { assertFalse(ec.addEmployee(e));

            assertEquals(employeeNo, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC3_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("Ionel");
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(6001d);
        try {assertFalse( ec.addEmployee(e));

            assertEquals(employeeNo, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC4_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName(null);
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500d);
        try { assertFalse(ec.addEmployee(e));

            assertEquals(employeeNo, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC5_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("");
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500d);
        try { assertFalse(ec.addEmployee(e));

            assertEquals(employeeNo , ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC6_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("IONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNn");
        e.setLastName("Pacurariu");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(2500d);
        try {assertFalse(ec.addEmployee(e));

            assertEquals(employeeNo , ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC7_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("I");
        e.setLastName("Pa");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(1000d);
        try { ec.addEmployee(e);

            assertEquals(employeeNo + 1, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC8_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("Io");
        e.setLastName("Pa");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(1001d);
        try { ec.addEmployee(e);

            assertEquals(employeeNo + 1, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC9_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("IONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN");
        e.setLastName("Pa");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(5999d);
        try { ec.addEmployee(e);

            assertEquals(employeeNo + 1, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @Test
    void addEmployeeTC10_BB() {
        int employeeNo = ec.getEmployeeList().size();
        System.out.println("Nr. Angajati actual = " + ec.getEmployeeList().size());
        Employee e=new Employee();
        e.setFirstName("IONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNn");
        e.setLastName("Pa");
        e.setCnp("1234567890876");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(5999d);
        try { ec.addEmployee(e);

            assertEquals(employeeNo + 1, ec.getEmployeeList().size());

        } catch (Exception exception){
            exception.printStackTrace();
        }
        System.out.println("Nr. angajati nou = " + ec.getEmployeeList().size());
    }
    @AfterEach
    void tearDown() {
    }
}